#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
     name="lanshark-django-brutebuster",
     version="0.2.0",
     packages=find_packages(),
     author="LANshark Consulting Group, LLC.",
     author_email="ssharkey@lanshark.com",
     description="Pluggable Django application providing password bruteforce protection",
     url="https://gitlab.com/lansharkconsulting/django/ls-django-brutebuster/",
     include_package_data=True,
     zip_safe=False
)
