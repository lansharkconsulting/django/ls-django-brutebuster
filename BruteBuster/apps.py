from django.apps import AppConfig


class BrugetBusterConfig(AppConfig):
    name = 'BruteBuster'
